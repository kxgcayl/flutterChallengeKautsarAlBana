import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final authenticationProvider = ChangeNotifierProvider<AuthenticationRepository>(
  (_) => AuthenticationRepository(),
);

class AuthenticationRepository extends ChangeNotifier {
  User? user;
  bool status = false;
  List<User> userDatabase = [
    User('kautsaralbana', '123'),
  ];

  Future<bool> login(User data) async {
    final loginData = userDatabase.where((e) {
      return e.userID == data.userID && e.password == data.password;
    }).toList();
    if (loginData.isNotEmpty) fetch(data);
    return status;
  }

  Future<void> fetch(User data) async {
    user = User(data.userID, data.password);
    status = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('login', status);
    notifyListeners();
  }

  Future<void> logout() async {
    await Future.delayed(const Duration(seconds: 2)).then((_) async {
      user = null;
      status = false;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('login', status);
      notifyListeners();
    });
  }

  Future<bool> register(User data) async {
    await Future.delayed(const Duration(seconds: 2)).then((_) {
      userDatabase.add(User(data.userID, data.password));
      notifyListeners();
    });
    return true;
  }
}

class User {
  User(this.userID, this.password);

  final String userID;
  final String password;

  @override
  String toString() {
    // ignore: unnecessary_brace_in_string_interps
    return 'userID:${userID} | password:${password}';
  }
}
