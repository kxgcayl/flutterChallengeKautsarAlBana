import 'package:lottie/lottie.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:impact_flutter_challenge/styles/colors.dart';
import 'package:impact_flutter_challenge/gen/assets.gen.dart';
import 'package:impact_flutter_challenge/providers/authentication_provider.dart';

import 'build_dialog.dart';

class LoginPage extends StatefulHookConsumerWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ConsumerState<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final _userIdCtrl = useTextEditingController(text: '');
    final _pwdCtrl = useTextEditingController(text: '');
    final _auth = ref.read(authenticationProvider);

    bool areFieldsEmpty() {
      return _userIdCtrl.text.toString().isEmpty ||
          _pwdCtrl.text.toString().isEmpty;
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          width: context.screenWidth,
          height: context.screenHeight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: context.screenWidth / 3,
                    child: Image(image: Assets.images.headerLogin),
                  ),
                  const SizedBox(width: 12),
                  Image(image: Assets.images.logo).w(140).h(140),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      'Login'.text.fontWeight(FontWeight.bold).size(22).make(),
                      const SizedBox(height: 6),
                      'Please sign in to continue'
                          .text
                          .fontWeight(FontWeight.w300)
                          .size(16)
                          .make(),
                    ],
                  ),
                  const SizedBox(height: 14),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      'User ID'.text.make(),
                      TextField(
                        controller: _userIdCtrl,
                        decoration: const InputDecoration(
                          hintText: 'User Id',
                          hintStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      'Password'.text.make(),
                      TextField(
                        controller: _pwdCtrl,
                        obscureText: true,
                        decoration: const InputDecoration(
                          hintText: 'Password',
                          hintStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                    ],
                  ),
                  'LOGIN'
                      .text
                      .color(Colors.white)
                      .size(18)
                      .fontWeight(FontWeight.w600)
                      .make()
                      .centered()
                      .capsule(
                        width: 160,
                        height: 45,
                        backgroundColor: IColors.primaryDark,
                      )
                      .onTap(() {
                        if (areFieldsEmpty()) {
                          buildDialog(
                            context,
                            message: 'User ID dan atau Password Belum Di Isi',
                            asset: Assets.others.errorAnimation,
                            btnOkOnPress: () {},
                          ).show();
                        } else {
                          _auth
                              .login(User(
                            _userIdCtrl.text.toString(),
                            _pwdCtrl.text.toString(),
                          ))
                              .then(
                            (result) {
                              if (result) {
                                buildDialog(
                                  context,
                                  message: 'Login Berhasil',
                                  asset: Assets.others.successAnimation,
                                  btnOkOnPress: () {
                                    context.vxNav
                                        .clearAndPush(Uri(path: '/home'));
                                  },
                                ).show();
                              }
                            },
                          );
                        }
                      })
                      .objectBottomRight()
                      .py(20)
                ],
              ).px(26),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  'Don\'t have an Account?'.text.make(),
                  const SizedBox(width: 5),
                  'Sign Up'
                      .text
                      .color(IColors.secondary)
                      .fontWeight(FontWeight.bold)
                      .make()
                      .onTap(() {
                    context.vxNav.clearAndPush(Uri(path: '/register'));
                  })
                ],
              ).pOnly(bottom: 20),
            ],
          ),
        ),
      ),
    );
  }
}
