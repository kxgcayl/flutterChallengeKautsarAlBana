import 'package:lottie/lottie.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

AwesomeDialog buildDialog(
  BuildContext context, {
  required String message,
  required Function? btnOkOnPress,
  required String asset,
}) {
  return AwesomeDialog(
    context: context,
    animType: AnimType.SCALE,
    dialogType: DialogType.NO_HEADER,
    body: Center(
      child: Column(
        children: [
          Lottie.asset(
            asset,
            width: 210,
            height: 210,
          ),
          message.text
              .fontWeight(FontWeight.w500)
              .align(TextAlign.center)
              .make()
              .px(60)
        ],
      ),
    ),
    btnOkOnPress: btnOkOnPress,
  );
}
