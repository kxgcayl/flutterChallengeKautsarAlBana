import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:impact_flutter_challenge/gen/assets.gen.dart';
import 'package:impact_flutter_challenge/providers/authentication_provider.dart';
import 'package:impact_flutter_challenge/styles/colors.dart';
import 'package:velocity_x/velocity_x.dart';

import 'build_dialog.dart';

class RegisterPage extends StatefulHookConsumerWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends ConsumerState<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    final _userIdCtrl = useTextEditingController(text: '');
    final _pwdCtrl = useTextEditingController(text: '');

    bool areFieldsEmpty() {
      return _userIdCtrl.text.toString().isEmpty ||
          _pwdCtrl.text.toString().isEmpty;
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          width: context.screenWidth,
          height: context.screenHeight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image(image: Assets.images.headerLogin)
                      .w(context.screenWidth / 3),
                  Image(image: Assets.images.logo).w(140).h(140),
                  const SizedBox(width: 12),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      'Register'
                          .text
                          .fontWeight(FontWeight.bold)
                          .size(22)
                          .make(),
                      const SizedBox(height: 6),
                      'Please register to get sign in access'
                          .text
                          .fontWeight(FontWeight.w300)
                          .size(16)
                          .make(),
                    ],
                  ),
                  const SizedBox(height: 14),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      'User ID'.text.make(),
                      TextField(
                        controller: _userIdCtrl,
                        decoration: const InputDecoration(
                          hintText: 'User Id',
                          hintStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      'Password'.text.make(),
                      TextField(
                        controller: _pwdCtrl,
                        obscureText: true,
                        decoration: const InputDecoration(
                          hintText: 'Password',
                          hintStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                    ],
                  ),
                  'SUBMIT'
                      .text
                      .color(Colors.white)
                      .size(18)
                      .fontWeight(FontWeight.w600)
                      .make()
                      .centered()
                      .capsule(
                        width: 160,
                        height: 45,
                        backgroundColor: IColors.primaryDark,
                      )
                      .onTap(() {
                        areFieldsEmpty()
                            ? buildDialog(
                                context,
                                message:
                                    'User ID dan atau Password Belum Di Isi',
                                asset: Assets.others.errorAnimation,
                                btnOkOnPress: () {},
                              ).show()
                            : ref
                                .read(authenticationProvider)
                                .register(User(
                                  _userIdCtrl.text.toString(),
                                  _pwdCtrl.text.toString(),
                                ))
                                .then((result) {
                                buildDialog(
                                  context,
                                  message: 'Regsitrasi Berhasil',
                                  asset: Assets.others.successAnimation,
                                  btnOkOnPress: () {
                                    context.vxNav.clearAndPush(
                                      Uri(path: '/login'),
                                    );
                                  },
                                ).show();
                              });
                      })
                      .objectBottomRight()
                      .py(20)
                ],
              ).px(26),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  'Already have an Account?'.text.make(),
                  const SizedBox(width: 5),
                  'Login'
                      .text
                      .color(IColors.secondary)
                      .fontWeight(FontWeight.bold)
                      .make()
                      .onTap(
                    () {
                      context.vxNav.replace(Uri(path: '/login'));
                    },
                  )
                ],
              ).pOnly(bottom: 20),
            ],
          ),
        ),
      ),
    );
  }
}
