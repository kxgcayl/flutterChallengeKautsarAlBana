import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:impact_flutter_challenge/providers/authentication_provider.dart';
import 'package:impact_flutter_challenge/styles/colors.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:impact_flutter_challenge/gen/assets.gen.dart';

import 'build_dialog.dart';

class HomePage extends ConsumerStatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Image(image: Assets.images.headerSplash).objectTopRight(),
          'LOGOUT'
              .text
              .color(Colors.white)
              .size(18)
              .fontWeight(FontWeight.w600)
              .make()
              .centered()
              .capsule(
                width: 160,
                height: 45,
                backgroundColor: IColors.secondary,
              )
              .shadowMax
              .onTap(() {
                ref.read(authenticationProvider).logout().then((_) {
                  buildDialog(
                    context,
                    message: 'Logout Berhasil',
                    asset: Assets.others.successAnimation,
                    btnOkOnPress: () {
                      context.vxNav.replace(Uri(path: '/login'));
                    },
                  ).show();
                });
              })
              .centered()
              .py(20),
        ],
      ),
    );
  }
}
