import 'package:flutter/material.dart';
import 'package:impact_flutter_challenge/gen/assets.gen.dart';
import 'package:velocity_x/velocity_x.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Image(image: Assets.images.headerSplash).objectTopRight(),
          Expanded(child: Image(image: Assets.images.logo)),
          Align(
            alignment: Alignment.bottomLeft,
            child: Image(image: Assets.images.footerSplash),
          )
        ],
      ),
    );
  }
}
