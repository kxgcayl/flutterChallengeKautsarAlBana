import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:impact_flutter_challenge/pages/home_page.dart';
import 'package:impact_flutter_challenge/pages/login_page.dart';
import 'package:impact_flutter_challenge/pages/register_page.dart';
import 'package:impact_flutter_challenge/pages/splash_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InitialPage extends ConsumerStatefulWidget {
  const InitialPage({Key? key}) : super(key: key);

  @override
  _InitialPageState createState() => _InitialPageState();
}

class _InitialPageState extends ConsumerState<InitialPage> {
  @override
  Widget build(BuildContext context) => ref.watch(splashProvider).when(
        data: (data) => data != true ? const LoginPage() : const HomePage(),
        loading: () => const SplashPage(),
        // Error is directing to login because isn't possible to error
        error: (error, s) => const LoginPage(),
      );
}

final splashProvider = FutureProvider<bool>((_) async {
  bool status = false;
  await Future.delayed(const Duration(seconds: 2)).then((_) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final isLogin = prefs.getBool('login');
    if (isLogin.toString() == 'true') status = true;
    if (isLogin.toString() == 'false') status = false;
  });
  return status;
});
