import 'package:flutter/material.dart';

class IColors {
  IColors._();

  static const Color primaryDark = Color(0xFF643b9c);
  static const Color primaryLight = Color(0xFF9c84d4);
  static const Color secondary = Color(0xFFec4c24);
}
