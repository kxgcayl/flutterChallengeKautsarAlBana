import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:impact_flutter_challenge/pages/home_page.dart';

import 'package:impact_flutter_challenge/pages/initial_page.dart';
import 'package:impact_flutter_challenge/pages/register_page.dart';
import 'package:impact_flutter_challenge/styles/colors.dart';
import 'package:velocity_x/velocity_x.dart';

import 'pages/login_page.dart';

void main() {
  Vx.setPathUrlStrategy();
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final _navigator = VxNavigator(
    notFoundPage: (uri, params) => MaterialPage(
      key: const ValueKey('not-found-page'),
      child: Builder(
        builder: (context) => Scaffold(
          body: Center(
            child: Text('Page ${uri.path} not found'),
          ),
        ),
      ),
    ),
    observers: [RouteObserver()],
    routes: {
      '/': (uri, params) => const MaterialPage(child: InitialPage()),
      '/login': (uri, params) => const MaterialPage(child: LoginPage()),
      '/register': (uri, params) => const MaterialPage(child: RegisterPage()),
      '/home': (uri, params) => const MaterialPage(child: HomePage()),
    },
  );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: 'VxNavigator',
        routerDelegate: _navigator,
        routeInformationParser: VxInformationParser(),
        theme: ThemeData(
          fontFamily: 'Poppins',
          primaryColor: IColors.primaryDark,
          colorScheme: const ColorScheme.light(
            primary: IColors.primaryDark,
            primaryVariant: IColors.primaryLight,
          ),
        ),
      ),
    );
  }
}

class RouteObserver extends VxObserver {
  @override
  void didChangeRoute(Uri route, Page page, String pushOrPop) {
    print("${route.path} - $pushOrPop");
  }

  @override
  void didPush(Route route, Route? previousRoute) {
    print('Pushed a route');
  }

  @override
  void didPop(Route route, Route? previousRoute) {
    print('Popped a route');
  }
}
