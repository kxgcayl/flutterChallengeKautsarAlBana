# Flutter Challenge Kautsar Al Bana

Flutter Challenge from Infosys Solusi Terpadu

![Application Pages](https://gitlab.com/kxgcayl/flutterChallengeKautsarAlBana/-/raw/main/assets/others/screenshots.png)

## Installation

1. Clone Repository Project

```sh
$ git clone https://gitlab.com/kxgcayl/flutterChallengeKautsarAlBana.git
```

2. Go to project repository

```sh
$ cd flutterChallengeKautsarAlBana
```

3. Download Resource

```sh
$ flutter pub get
```

4. Run Build Runner

```sh
$ flutter packages pub run build_runner build
```

5. Check that an Android device is running. If none are shown, follow the device-specific instructions on the [Install](https://flutter.dev/docs/get-started/install) page for your OS.

```sh
$ flutter devices
```

6. Run the app with the following command:

```sh
$ flutter run
```

7. Login with:

|    User Id    | Password |
| :-----------: | :------: |
| kautsaralbana |   123    |
